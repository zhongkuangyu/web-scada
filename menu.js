//Add in your page into this list
var menu = {
"Plant Process" : "strz.svg",
//"Plant Track" : "cim.svg",
"Candy" : "candy.svg",
"Oil and Gas" : {"Condensate Skid":"conskid.svg","Gas Skid":"gasskid.svg"},
"Transportation" : "trainMonitor.svg",
"Waste Water" : "wwtp.svg",//{"Sg. Udang":"su.svg","Kuala Sawah":"ks.svg","Pasir Gudang":"wwtp.svg"},
"Alarm": "alarm.htm",
"Trend": "plot.htm",
"Report": {
	"Statement Report": "report-stmt.htm",
	"Snapshot Report": "report-snap.htm"
},

//the last page/item must not be terminated by comma.
"About":"about.svg"
};

//Name your project title here.
var title = "Demo"; 

//Put your project logo, i.g. "images/mylogo.gif"
var logo = "system/images/v3-logo.gif"; 
