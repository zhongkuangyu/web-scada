# Ecava WEB SCADA

#### Description
Ecava Web Scada 凭借精益架构，快速处理，增强的多功能性和专家支持，Ecava IGX 被用于自来水、污水处理、环保、石油、天然气、电力、能源、钢厂、热力、数字工厂、采矿、楼宇、电信、化工、水电站、风电站、实验检测、设备物联、水库湖泊、农业、汽车、食品、注塑硫化、路灯远控、消防、制冷等行业。Ecava IGX 在全球范围内赢得了广泛的欢迎和快速增长的客户群。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
