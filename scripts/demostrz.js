
/***** Strz code start *****/

var rank = +getTag( 'app.server.rank');
if (rank>0) return;
    //Init, only need to do once. 



//isPadlCyorRun
var TANK_QTTY = 4;
var TIMER_CHARGE = 5;
var TIMER_COOK = 20;
var TIMER_DISC = 10;
var TIMER_DUMP = 8;

var WEIGHT_ACUM = 28000;
var WEIGHT_WATER = 20000;




function isFeeding( Tank) {
    var expected = 1;
    var pos = ((Tank + 1) % 2) * 10;
    var rite = +(getTag( 'isCyorFeedRunRite')) || 0;
    var left = +(getTag( 'isCyorFeedRunLeft')) || 0;
    switch (Tank) {
        case 1:
            expected = (left && pos == 1);
            break;
        case 2:
            expected = (left && pos == 10);
            break;
        case 3:
            expected = (rite && pos == 1);
            break;
        case 4:
            expected = (rite && pos == 10);
            break;
        default:
            expected = 1;
            break;
    }
    return expected;
}

//Before the feeding belt is running, 
//position must be fixed, the needed door must be opened.
//When a targeted door is not open, Belt shall not run.
function runCyorFeed( Tank) {

    var result;
    var isCyorFeedRunRite = +(getTag( 'isCyorFeedRunRite')) || 0;
    var isCyorFeedRunLeft = +(getTag( 'isCyorFeedRunLeft')) || 0;
    
    var isChrgDoorOpn = +(getTag( 'isChrgDoorOpn' + Tank)) || 0;
    var posCyor = +(getTag( 'posCyor')) || 0;
    var pos_dst = ((Tank + 1) % 2) * 10 + ((Tank + 1) % 2);
    posCyor = posCyor.toFixed(); //trim all decimal point.
    //debugString( 'decimal: ' + pos_dst);

    if (!isCyorFeedRunRite && !isCyorFeedRunRite
            && (posCyor != pos_dst)) {
        setPosCyor( posCyor, pos_dst);
    }
    else if (posCyor == pos_dst && isChrgDoorOpn) {

        var direction = (Tank > 2)? 1: 0;
        setTag( 'isCyorFeedRunRite', direction);
        setTag( 'isCyorFeedRunLeft', !direction);
        setTag( 'isCyorPadlRun', 1);
        result = 1;
    }
    return result;
}

function setPosCyor( Pos, Dst) {
    var pos = Pos;
    if (Pos > Dst) {
        pos--;
    }
    else if (Pos < Dst) {
        pos++;
    }
    setTag( 'isCyorFeedRunRite', 0);
    setTag( 'isCyorFeedRunLeft', 0);                
    setTag( 'posCyor', pos);
    ///debugString( 'setPosCyor(); pos: ' + pos + ', dst: ' + Dst);
}

function isCyorBusy( Tank) {
    var seq_process;
    var is_busy;
    for (var i = 1; i <= TANK_QTTY; i++) {
        seq_process = +(getTag( 'seq_process' + i)) || 0;
        if (1 == seq_process) {
            is_busy = 1;
            break;
        }
    }
    return is_busy;
}
///////////////////////////////////////////////////////////////////////

var isDischarging = 0;
for (i = 1; i <= TANK_QTTY; i++) {
    
    //Interlock to stop feeding if chrg door is closed.
    var isChrgDoorOpn = +(getTag( 'isChrgDoorOpn' + i) || 0);
/*    if (isFeeding( i) && !isChrgDoorOpn) {
        setTag( 'isCyorFeedRunLeft', 0);
        setTag( 'isCyorFeedRunRite', 0);
    }
*/
    var req_process = +(getTag( 'req_process' + i)) || 0;
    var seq_process = +(getTag( 'seq_process' + i)) || 0;
    var seq_timer = +(getTag( 'seq_timer' + i)) || 0;
    if (req_process != seq_process) { //next stage
        seq_timer = 0;                //reset timer
    }
    ///debugString( 'timer: ' + seq_timer);
 
    if (req_process != 1 || !isCyorBusy()) { //not booked by others
        seq_process = req_process;
    }

    
    switch (seq_process) {

        case 1://'charge':
            setTag('isDiscDoorOpn' + i, 0);
            setTag('isDiscDoorLock' + i, 1);
            setTag('isVdaOpn' + i, 0);
            setTag('isVsaOpn' + i, 0);
            setTag('isVsbOpn' + i, 0);
            setTag('isVvaOpn' + i, 0);
            setTag('isVvbOpn' + i, 0);
            setTag('isAugerIn' + i, 0);
            setTag('isAugerRun' + i, 0);
                        
            setTag('isChrgDoorLock' +i, 0);
            setTag('isChrgDoorOpn' +i, 1);
            
            ////setTag('posCyorFeed', 0 + vessel;
            if (runCyorFeed( i)) {
                seq_timer++;
                var weight_acum = (seq_timer/6 * WEIGHT_ACUM - Math.random()*100).toFixed();
                setTag('weight_acum' + i, weight_acum);
                setTag('weight_fruit' + i, weight_acum - WEIGHT_WATER);
                if (seq_timer > TIMER_CHARGE) {
                    //next stage.
                    seq_timer = 0;
                    setTag( 'isCyorFeedRunLeft', 0);
                    setTag( 'isCyorFeedRunRite', 0);
                    setTag( 'isCyorPadlRun', 0);
        
                    setTag( 'weight_acum_ref' + i, weight_acum);
                    setTag( 'weight_fruit_ref' + i, weight_acum - WEIGHT_WATER);
                    req_process++;
                }
            }
            break;

        case 2://'cook':

            setTag('isChrgDoorOpn' + i, 0);
            setTag('isDiscDoorOpn' + i, 0);
            setTag('isChrgDoorLock' + i, 1);
            setTag('isDiscDoorLock' + i, 1);
            setTag('isAugerIn' + i, 1);
            setTag('isAugerRun' + i, 1);

            setTag('isVsaOpn' + i, 1);
            setTag('isVsbOpn' + i, 1);
            
            setTag('isVdaOpn' + i, 0);
            setTag('isVvaOpn' + i, 0);
            setTag('isVvbOpn' + i, 0);

            seq_timer++;
            //var CurrTime = seq_timer * (Math.random()*10).toFixed();
            var CurrTime = seq_timer;// * abs(Math.random()*10).toFixed();
            setTag( 'CurrTime' + i, CurrTime);
            setTag( 'LeftTime' + i, TIMER_COOK - CurrTime  );
            setTag( 'Press' + i, (1+(seq_timer * Math.random()*10)/10).toFixed());
            if (seq_timer >= TIMER_COOK) {
                //next stage.
                seq_timer = 0;
                req_process++;
             
            }
            break;

        case 3://'discharge':
    //Discharge:
    //==========
    //Start Disc cyor. (1s)
    //Unlock all, open Discharge door. (1s)
            isDischarging = 1;
            setTag('isVsaOpn' + i, 0);
            setTag('isVsbOpn' + i, 0);

            setTag('isVvaOpn' + i, 1);
            setTag('isVvbOpn' + i, 1);
            
            setTag('isAugerIn' + i, 0);
            setTag('isAugerRun' + i, 0);

            setTag('isDiscDoorOpn' + i, 1);
            setTag('isDiscDoorLock' + i, 0);
            setTag( 'Press' + i, 0);
            
            seq_timer++;
            var weight_acum_ref = +(getTag( 'weight_acum_ref' + i));
            var weight_fruit_ref = +(getTag( 'weight_fruit_ref' + i));
            var diff = weight_acum_ref - weight_fruit_ref;
            diff = diff * seq_timer / TIMER_DISC;
            
            //var weight_acum = +(getTag( 'weight_acum_re' + i)) - diff;
            
            setTag( 'weight_acum' + i, weight_acum_ref - diff);
            
            if (seq_timer >= TIMER_DISC) {
                //next stage.
                seq_timer = 0;
                setTag( 'weight_acum_ref' + i,  weight_acum_ref - diff);
                req_process++;
            }
            break;




        case 4://'dump':
            setTag('isVdaOpn' + i, 1);
            
            seq_timer++;
            var weight_acum_ref = getTag( 'weight_acum_ref' + i);
            var weight_fruit_ref = getTag( 'weight_fruit_ref' + i);
            setTag( 'weight_acum' + i, (weight_acum_ref * (TIMER_DUMP- seq_timer) / TIMER_DUMP).toFixed());
            setTag( 'weight_fruit' + i, (weight_fruit_ref * (TIMER_DUMP- seq_timer) / TIMER_DUMP).toFixed());
            if (seq_timer >= TIMER_DUMP) {
                //repeat stage.
                
                seq_timer = 0;
                req_process = 1;
                seq_process = 0;
            }

            break;

        case 0://'ready': //fall thru
        default:
            seq_timer = 0;

            break;
        
    }


    
    setTag( 'req_process' + i, req_process);
    setTag( 'seq_process' + i, seq_process);

    setTag( 'seq_timer' + i, seq_timer);
    req_process = +(getTag( 'req_process' + i) || 168);
    seq_process = +(getTag( 'seq_process' + i) || 328);

}   //end of tank for loop

setTag( 'isCyorDiscRun', isDischarging);

//debugString("igrx: end - demostrz.js");

/***** Strz code end *****/
