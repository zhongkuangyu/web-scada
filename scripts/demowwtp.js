
/***** Waste Water Treatment Plant code start *****/
/// Conversion Function //////////////////////////////////////////////////////////////////////////////////
function dataConversion(tag, min, max) {
    var a = getTag('raw_'+tag);
    var b = (a-6400)*(max-min)/25600;
    var c = b+min;
    setTag( tag , c);
}

dataConversion('effStor', 0, 100);
dataConversion('neuSump', 0, 100);
dataConversion('inFlow1', 0, 30);
dataConversion('inFlow2', 0, 30);
dataConversion('effFlow', 0, 70);
dataConversion('effPH', 4, 10);
dataConversion('effTemp', 0, 100);

// wwdp
if ((1 == getTag('valve_2_open'))||(1 == getTag('valve_3_open'))) {
	setTag('wwdp_1_cmd', 1);
	setTag('wwdp_1_run', 1);
} else {
	setTag('wwdp_1_cmd', 0);
	setTag('wwdp_1_run', 0);
}
	
// valve
var valve = ['valve_1','valve_2','valve_3','valve_4','valve_5'];
for (i in valve) {
	if (1 == getTag(valve[i]+'_cmd')) {
    	setTag(valve[i]+'_open', 1);
		setTag(valve[i]+'_close', 0);
	} else {
		setTag(valve[i]+'_open', 0);
		setTag(valve[i]+'_close', 1);
	}
}

// effStor
if (getTag('set_effStor_LL') > getTag('effStor')) {
	setTag('raw_effStor', getTag('raw_effStor')+111);
	setTag('effStor_LL', 1);
} else {
	setTag('effStor_LL', 0);
	if (0 == getTag('valve_1_open')) {
		setTag('raw_effStor', getTag('raw_effStor')+22);
	} else {
		setTag('raw_effStor', getTag('raw_effStor')-33);
	}
}
if (getTag('set_effStor_L') < getTag('effStor')) {
	setTag('effStor_L', 1);
	if ((0 == getTag('neuSump_H'))&&(0 == getTag('valve_2_open'))&&(0 == getTag('valve_3_open'))) {
		setTag('valve_1_cmd', 1);
	} else {
		setTag('valve_1_cmd', 0);
	}
} else {
	setTag('effStor_L', 0);
    setTag('valve_1_cmd', 0);
}
if (getTag('set_effStor_H') < getTag('effStor')) {
	setTag('effStor_H', 1);
} else {
	setTag('effStor_H', 0);
}
if (getTag('set_effStor_HH') < getTag('effStor')) {
	setTag('raw_effStor', getTag('raw_effStor')-999);
	setTag('effStor_HH', 1);
} else {
	setTag('effStor_HH', 0);
}

// neuSump
if ((1 == getTag('valve_1_open'))||(1 == getTag('valve_4_open'))||(1 == getTag('valve_5_open'))) {
	setTag('raw_neuSump', getTag('raw_neuSump')+111);
}
if (getTag('set_neuSump_LL') > getTag('neuSump')) {
	setTag('neuSump_LL', 1);
} else {
	setTag('neuSump_LL', 0);
}
if (getTag('set_neuSump_L') < getTag('neuSump')) {
	setTag('neuSump_L', 1);
} else {
	setTag('neuSump_L', 0);
	setTag('valve_3_cmd', 0);
	if (19200 > getTag('raw_effPH')) {
		setTag('raw_effPH', 32100);
	} else {
		setTag('raw_effPH', 6420);
	}
}
if (getTag('set_neuSump_H') < getTag('neuSump')) {
	setTag('neuSump_H', 1);
	if (0 == getTag('valve_3_open')) {
		setTag('valve_2_cmd', 1);
	}  
} else {
	setTag('neuSump_H', 0);
}
if (getTag('set_neuSump_HH') < getTag('neuSump')) {
	setTag('raw_neuSump', getTag('raw_neuSump')-999);
	setTag('neuSump_HH', 1);
} else {
	setTag('neuSump_HH', 0);
}
if ((1 == getTag('valve_3_open'))&&(1 == getTag('wwdp_1_run'))) {
	setTag('raw_neuSump', getTag('raw_neuSump')-333);
	setTag('valve_2_cmd', 0);
}

// totalizer    
var totTag = ['inFlow1', 'inFlow2', 'effFlow'];
for (var i in totTag) {
	var ctr = getTag(totTag[i]+'_C');
	if (isNaN(ctr) || ctr > 1000) {
		setTag(totTag[i]+'_C', 0);
	} else {
		if (ctr >= 1000) { setTag(totTag[i]+'_Total', getTag(totTag[i]+'_Total')+1);}
		if (getTag('raw_'+totTag[i]) > 19200) { setTag(totTag[i]+'_C', ++ctr);}
	}
}
    
// flow
setTag('raw_inFlow1', getTag('raw_effStor'));
setTag('raw_inFlow2', getTag('raw_neuSump'));
if (1 == getTag('wwdp_1_run')) {
	setTag('raw_effFlow', getTag('raw_effFlow')+11);
	if (32000 < getTag('raw_effFlow')) {
		setTag('raw_effFlow', 12345);
	}
} else {
	setTag('raw_effFlow', 6400);
}

// ph & temp
if (1 == getTag('wwdp_1_run')) {
	if (getTag('set_effPH_L') > getTag('effPH')) {
		setTag('valve_5_cmd', 1);
		setTag('raw_effPH', getTag('raw_effPH')+11);
		setTag('raw_effTemp', getTag('raw_effTemp')+22);
	} else if (getTag('set_effPH_H') < getTag('effPH')) {
		setTag('valve_4_cmd', 1);
		setTag('raw_effPH', getTag('raw_effPH')-11);
		setTag('raw_effTemp', getTag('raw_effTemp')+22);
	} else {
		if ((getTag('set_effTemp_H') > getTag('effTemp'))&&(getTag('set_neuSump_L') < getTag('neuSump'))) {
			setTag('valve_2_cmd', 0);
			setTag('valve_3_cmd', 1);
			setTag('batchCounter', getTag('batchCounter')+1);
		}
		setTag('valve_4_cmd', 0);
		setTag('valve_5_cmd', 0);
		setTag('raw_effTemp', getTag('raw_effTemp')-22);
	}
} else {
	setTag('valve_4_cmd', 0);
	setTag('valve_5_cmd', 0);
} 
    
//water animation
var ctr = getTag('animeFlow');
ctr = parseFloat( ctr);
if (isNaN( ctr) || ctr > 4) { ctr = -1;}
setTag( 'animeFlow', ++ctr);
    
//pump blade animation
 var ctr = getTag( '_ctr360');
 ctr = parseFloat( ctr);
 if (isNaN( ctr) || ctr > 360) { ctr = 0;}
 setTag( '_ctr360', ++ctr);
/***** Waste Water Treatment Plant code end *****/