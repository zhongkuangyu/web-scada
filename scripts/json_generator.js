// An example to generate a JSON string from tag values
(function() {
	try {

		var count = igx.count(); // defined in counter.js

	} catch (err) {

		debugString(err.message);

		return;

	}

	var date = getTag("app.currentTime.date");
	var time = getTag("app.currentTime.time");
	var pressure = getTag("pressure_cook");
	var temperature = getTag("temperature_cook");
	var timestamp = date + " " + time;


	var names = ["timestamp", "count", "pressure", "temperature"];
	var values = [timestamp, count, pressure, temperature];

	var n = values.length;
	if (n != names.length)
		return;

	var last = n - 1;
	var i, isString = false;

	var jsonString = "{\n";
	for (i = 0; i < n; ++i) {
		// name or property
		jsonString += '"';
		jsonString += names[i];
		jsonString += '"';

		jsonString += ":";

		// value
		addQuote = (typeof values[i] != "number");
		if (addQuote)
			jsonString += '"';

		jsonString += values[i];

		if (addQuote)
			jsonString += '"';

		if (i < last)
			jsonString += ",";

		jsonString += "\n";
	}
	jsonString += "}";

	setTag("virtual_json", jsonString);
})();