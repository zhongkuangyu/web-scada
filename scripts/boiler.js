var list = {
'mqtt_sim_temp': 2,
'Lower_Tank': 100,
'Upper_Tank': 55,
'Deaerator_Tank': 70,
'Vent_Valve': 6,
'Valve_1': 15,
'Valve_2': 30,
'Valve_3': 100,
'Valve_4': 25,

'Radiant': 495,

'Secondary': 530,

'Primary': 510,


'Economiser': 150,
'Air_Preheater': 100
};

for (var i in list) {
    var data = getTag(i) || Math.random()*list[i];
	data = getProcessData( data, list[i]);
	setTag( i , data);
	debugTag( i); //this line print value of tag in debugging mode.
}

function getProcessData( prev, range) {
    var rnd = Math.random()*5;
	if ((prev + rnd)> range) {
	    rnd = prev - rnd;
	}
	else {
	    rnd = prev + rnd;
    }
    return rnd;
}

//agitator rotation animation

var ctr = getTag( 'pos');

if (ctr < 4) {

ctr++;

}

else {

ctr = 1;

}

setTag( 'pos', ctr);

switch (ctr) {

case 1:

setTag( 'A', 1);

setTag( 'B', 0);

setTag( 'C', 0);


setTag( 'D', 0);

break;

case 2:


setTag( 'A', 0);


setTag( 'B', 1);


setTag( 'C', 0);


setTag( 'D', 0);

break;

case 3:


setTag( 'A', 0);


setTag( 'B', 0);


setTag( 'C', 1);

setTag( 'D', 0);

break;


case 4:



setTag( 'A', 0);



setTag( 'B', 0);



setTag( 'C', 0);


setTag( 'D', 1);


break;

}


if (getTag('app.currentTime.second') < 10) {
	setTag('Pump_Run', false);
	if (getTag('app.currentTime.minute') < 1) {
		setTag('Pump_Trip', true);
	}
}
else {
	setTag('Pump_Run', true);
	setTag('Pump_Trip', false);
}