function randomDataProcess(previous , tolerance) {
    var randomtolerance = Math.random()*tolerance;
    var random = Math.random();
    if(random<0.6) {
        return(previous*(100+randomtolerance)/100);
    }
    else {
        return(previous*(100-randomtolerance)/100);
    }
}

var TANK_QTTY = 4;
for (var i = 1; i <= TANK_QTTY; i++) {
    setTag( 'tank_number' + i, i);
    setTag( 'req_process' + i, i);
    setTag( 'isChrgDoorOpn' + i, 0);
    
}

var list = {
    'door_position': 0,    //default to open
    'drain_valve': 1,
    
    'level_cook': 33.3,
    'level_syrup': 99.9,
    'level_vanilla': 99.9,
    'level_chocolate': 99.9,
    'level_strawberry': 99.9,

    'recipe_syrup': 10.1,
    'recipe_vanilla': 12.3,
    'recipe_chocolate': 50.5,
    'recipe_strawberry': 10.7,
    
    // wwtp
    'sysFault' : 0,
    'wwdp_1_cmd' : 0,
    'wwdp_2_cmd' : 0,
    'valve_1_cmd' : 0,
    'valve_2_cmd' : 0,
    'valve_3_cmd' : 0,
    'valve_4_cmd' : 0,
    'valve_5_cmd' : 0,
    'inFlow1_DI' : 0,
    'inFlow2_DI' : 0,
    'effFlow_DI' : 0,
    'autoSelector' : 1,
    'wwdp_1_run' : 0,
    'wwdp_2_run' : 0,
    'wwdp_1_trip' : 0,
    'wwdp_2_trip' : 1,
    'valve_1_open' : 0,
    'valve_1_close' : 1,
    'valve_2_open' : 0,
    'valve_2_close' : 1,
    'valve_3_open' : 0,
    'valve_3_close' : 1,
    'valve_4_open' : 0,
    'valve_4_close' : 1,
    'valve_5_open' : 0,
    'valve_5_close' : 1,
    'effPH_H' : 0,
    'effPH_L' : 0,
    'effTemp_H' : 0,
    'sysReset' : 0,
    'sysRun' : 1,
    'correctionTime' : 0,
    'wwdp_1_auto' : 1,
    'wwdp_2_auto' : 0,
    'emerStop' : 0,
    'inFlow1_Total': 0,
    'inFlow2_Total': 0,
    'effFlow_Total': 0,
    'batchCounter': 0,
    'raw_effStor' : 23456,
    'raw_neuSump' : 23456,
    'raw_inFlow1' : 6400,
    'raw_inFlow2' : 6400,
    'raw_effFlow' : 6400,
    'raw_effPH' : 23456,
    'raw_effTemp' : 23456,   
    'isDemo': 1,
    
    // og
    'cMov_Inlet1' : 1,
    'cMov_Outlet1' : 1,
    'pMov_Inlet1' : 0,
    'pMov_Outlet1' : 0,
    'cFcv_1' : 1,
    'cMov_Inlet2' : 0,
    'cMov_Outlet2' : 0,
    'pMov_Inlet2' : 0,
    'pMov_Outlet2' : 0,
    'cFcv_2' : 0,
    'cMov_Inlet3' : 1,
    'cMov_Outlet3' :1,
    'pMov_Inlet3' : 0,
    'pMov_Outlet3':0,
    'cFcv_3' : 1,
    'cMov_Inlet4' : 1,
    'cMov_Outlet4' : 0,
    'pMov_Inlet4' : 1,
    'pMov_Outlet4' : 1,
    'cFcv_4' : 1,
    
    'gMov_Inlet1' : 1,
    'gFcv_1' : 1,
    'gMov_Outlet1' : 1,
    'gMov_Inlet2' : 1,
    'gFcv_2' : 1,
    'gMov_Outlet2' : 1,
    'gMov_Inlet3' : 1,
    'gFcv_3' : 1,
    'gMov_Outlet3' : 1,

	//train
	'tren1_run': 1,
	'tren2_run': 1,
	'tren2_counter': 2300,
	'tren3_run': 0,
	'tren4_run': 0,
	'tren4_counter': 2300,
	'tren5_run': 0,
	'tren6_run': 0,
	'tren6_counter': 2300,
	'tren7_run': 1,
	'tren8_run': 0,
	'tren9_run': 1,
	'tren9_counter': 1400,
	'tren10_run': 1,
	'tren11_run': 1,
	'tren11_counter': 3400,
	'tren12_run': 0,
	'tren13_run': 0,
	'tren13_counter': 3400,
	'tren14_run': 0,
	'tren15_run': 0,
	'tren15_counter': 3400,
	'tren16_run': 1,
	'tren17_run': 1,
	'tren17_counter': 1800,
	'ERL': 0,
	'KTM_Seremban': 0,
	'KTM_Sentul': 0,

    '_init': 1 //last item no trailing comma.
};


    //Candy
    for (var i in list) {
        setTag( i, list[i]);
    }

    setTag('set_effStor_HH', 90);
    setTag('set_effStor_H', 80);
    setTag('set_effStor_L', 30);
    setTag('set_effStor_LL', 20);
    setTag('set_neuSump_HH', 90);
    setTag('set_neuSump_H', 80);
    setTag('set_neuSump_L', 30);
    setTag('set_neuSump_LL', 20);
    setTag('set_effPH_H', 9);
    setTag('set_effPH_L', 5.5);
    setTag('set_effTemp_H', 40);
    setTag('set_effFlow_L', 3);
    setTag('set_wwdp_runTimer', 15);
    setTag('set_PH_bandTimer', 15);
    setTag('set_V5_openTimer', 30);
    setTag('set_V4_openTimer', 30);
    setTag('set_correctionTimer', 30);

	var tren = ["tren1_","tren2_","tren3_","tren4_","tren5_","tren6_","tren7_","tren8_","tren9_","tren10_","tren11_","tren12_","tren13_","tren14_","tren15_","tren16_","tren17_"];
	for (var i = 0; i < tren.length; i++) {
		setTag(tren[i]+'A', 0);
		setTag(tren[i]+'B', 0);
		setTag(tren[i]+'C', 0);
		setTag(tren[i]+'D', 0);
		setTag(tren[i]+'E', 0);
		setTag(tren[i]+'F', 0);
	}



/***** Oil & Gas code start *****/
if (getTag("app.server.primary")) {

    var list = {
    'HV4060_ConstantSV':1000,
    'HV4060_Mfactor':100,
    'HV4060_Pfactor':10,
    'HV4060_Ifactor':10,
    'HV4060_ManualSetValue':100,
    'HV4060_Auto_DO':1,
    'HV4060_ModeSelect':1,
    'HV4063_ConstantSV':1000,
    'HV4063_Mfactor':100,
    'HV4063_Pfactor':10,
    'HV4063_Ifactor':10,
    'HV4063_ManualSetValue':100,
    'HV4063_Auto_DO':1,
    'HV4063_ModeSelect':1,
    'HV4066_ConstantSV':1000,
    'HV4066_Mfactor':100,
    'HV4066_Pfactor':10,
    'HV4066_Ifactor':10,
    'HV4066_ManualSetValue':100,
    'HV4066_Auto_DO':1,
    'HV4066_ModeSelect':1
    };
    var data = 0;
    for (var i in list) {
        if ((isNaN(getTag(i)))||(getTag(i)==0)) {
            data = Math.random()*list[i];
        } else {
            data = randomDataProcess( getTag(i) , 10);
        }
        if(list[i]==1) {
            if(data<0.5) { data = 0; } else { data = 1; }
        }
        setTag( i, data);
    }
    if ((1 == getTag('HV4060_Auto_DO'))||(0 < getTag('HV4060_ManualSetValue'))) {
        setTag('gFcv_1', 0);
    } else {
        setTag('gFcv_1', 1);
    }
    if ((1 == getTag('HV4063_Auto_DO'))||(0 < getTag('HV4063_ManualSetValue'))) {
        setTag('gFcv_2', 0);
    } else {
        setTag('gFcv_2', 1);
    }
    if ((1 == getTag('HV4066_Auto_DO'))||(0 < getTag('HV4066_ManualSetValue'))) {
        setTag('gFcv_3', 0);
    } else {
        setTag('gFcv_3', 1);
    }

}

///// Report Data Bgn /////
var list = {
	'part_a': 100,
	'part_b': 100,
	'part_c': 100,
	'part_d': 100
};
for (var i in list) {
    var data = getTag(i) || Math.random()*list[i];
	data = getProcessData( data, list[i]);
	setTag( i , data);
}

function getProcessData( prev, range) {
    var rnd = Math.random()*5;
	if ((prev + rnd)> range) {
	    rnd = prev - rnd;
	}
	else {
	    rnd = prev + rnd;
    }
    return rnd;
}

setTag('temperature_cook', getProcessData(getTag('temperature_cook'), 30));
setTag('effFlow', getProcessData(getTag('effFlow'), 20));
setTag('effTemp', getProcessData(getTag('effTemp'), 30));

//Data for horizontal Report
var horizontalReportData = {
	'totalConsumption':1167677,
	'PlantA': 184511,
	'buswayLV': 539928,
	'equipUPS': 1470,
	'genUPS': 1867,
	'PlantB': 38323,
	'DataCenter': 30333,
	'ConnectedLoads': 4563,
	'chillerUnits': 197177,
	'chillerCP': 414,
	'MAU': 36456,
	'PCW': 32088,
	'CWP': 14736,
	'CWSP': 7752,
	'CTF': 12960,
	'CTMF': 150,
	'CWPP': 11352,
	'RCU': 3776,
	'AHU': 3936,
	'FFU': 9344,
	'scrubberExhaust': 16608,
	'SRP': 1528,
	'generalExhaust': 2643,
	'solventExhaust': 1384,
	'WWTP': 456,
	'PWP': 539,
	'TCM': 600,
	'TGM': 466,
	'PVP': 3365,
	'lighting': 9035,
	'boiler': 743,
	'DWP': 151,
	'CCTV': 221,
	'scada': 566
	
};

for (var i in horizontalReportData) {
    var data = horizontalReportData[i] + (horizontalReportData[i]*0.2*Math.random());
	setTag( i , data);
}

var dummylist = {
	'dummyText': 'KWH Per Day'
};

for (var i in dummylist) {
	setTag( i , dummylist[i] );
}

///// Report Data End /////
