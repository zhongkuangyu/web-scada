


/***** Plant track code start *****/
var rank = +getTag( 'app.server.rank');
if (rank>0) return;

var mod1 = getTag('cim_mod1'), mod2 = getTag('cim_mod2'), mod3 = getTag('cim_mod3'),
    raw = getTag('cim_UM'), finish = getTag('cim_PP'), 
    run = parseFloat(getTag('run')),
    ctr = getTag('cim_counter'),
    barcode_ctr = getTag('bar_ctr') + 1,
    calendar = new Date(),
    date = checkDate(calendar.getDate()),
    month = checkDate(calendar.getMonth() + 1),
    year = calendar.getYear(),
    anime = getTag('anime'),
    UM = 0, EP = 0, PP = 0,
    list = [], value = 0, data = 0, pos, barcode; 
    
var cmd = "select tag_name from persistence where tag_id < 38 and 12 < tag_id";
var sql = getSql(cmd, "");
sql = sql.split('\n');

var cmd2 = "select tag_name from persistence where tag_id < 63 and 37 < tag_id";
var sql2 = getSql(cmd2, "");
sql2 = sql2.split('\n');
asrs_count();


/* Rack for Raw Material - store */
var r_list = [ 
    'AX1Y1', 'AX1Y2', 'AX1Y3', 'AX1Y4', 'AX1Y5', 
    'AX2Y1', 'AX2Y2', 'AX2Y3', 'AX2Y4', 'AX2Y5',
    'AX3Y1', 'AX3Y2', 'AX3Y3', 'AX3Y4', 'AX3Y5',
    'BX3Y1', 'BX3Y2', 'BX3Y3', 'BX3Y4', 'BX3Y5',
    'BX4Y1', 'BX4Y2', 'BX4Y3', 'BX4Y4', 'BX4Y5'
]

/* Rack for Finish Goods - store */
var f_list = [ 
    'CX1Y1', 'CX1Y2', 'CX1Y3', 'CX1Y4', 'CX1Y5', 
    'CX2Y1', 'CX2Y2', 'CX2Y3', 'CX2Y4', 'CX2Y5',
    'CX3Y1', 'CX3Y2', 'CX3Y3', 'CX3Y4', 'CX3Y5',
    'BX1Y1', 'BX1Y2', 'BX1Y3', 'BX1Y4', 'BX1Y5',
    'BX2Y1', 'BX2Y2', 'BX2Y3', 'BX2Y4', 'BX2Y5'
]

/* This script is creating 5 items at infeed area */
var qty = getTag("infeed.qty"), item_id = getTag("item_id");

if (qty < 5) {

    for (var i=1; i<=5; i++){
        var id = checkTime(item_id);
        if(getTag("infeed["+i+"].item") == ""){
            setTag("infeed["+i+"].item", "Item"+id);
            setTag("infeed["+i+"].item.status", 0); // 0 = unprocessed; 1 = processed
			//setTag("infeed["+i+"].item.location", " ");
			//setTag("infeed["+i+"].item.barcode", " ");
            setTag("item_id", item_id+1);
            item_id++;
        }
    }
}

if (item_id >= 1000) setTag("item_id", 1);
if (!getTag("cim_motor1")) setTag("cim_motor1", 1);
if (!getTag("cim_motor2")) setTag("cim_motor2", 1);
if (!getTag("cim_motor3")) setTag("cim_motor3", 1);
if (!getTag("cim_motor4")) setTag("cim_motor4", 1);

if (!mod1 && !mod2 && !mod3) setTag("cim_mod1", 1);
else if (mod1) mod1_SRM();
else if (mod2) mod2_MRM();
else if (mod3) mod3_RAP();

function mod1_SRM() {

    if (0 == anime) {
        if (ctr < 9) {
            ctr++;
        } else {
            ctr = 1;
        }
    }
    setTag( 'cim_counter', ctr);

    switch (ctr) {

        case 1:
        setTag('anime', 1);
        if (run < 2) {
            run++;
            setTag('infeed_pallet', run);
        } else {
            run = 0;
            setTag('infeed_pallet', 3);
            setTag('anime', 0);
        }
        setTag('run', run);
        break;
        
        case 2:
        setTag('infeed_pallet', 0);
        setTag('infeedToVS', getTag("infeed[1].item"));
        setTag('cim_stopper_CY2', 0);
        setTag('cim_stopper_CY3', 1);
        break;
        
        case 3:
        setTag('cim_stopper_CY2', 1);
        setTag('cim_barcode', "");
        setTag('anime', 1);
        if (run < 1) {
            run++;
        } else {
            run = 0;
            setTag('anime', 0);
        }
        setTag('run', run);
        setTag('cim_inspection_result', 1);
        break;
        
        case 4:
        setTag('VSToPreDis', getTag("VSystem[1].item"));
        setTag('cim_inspection_result', 0);
        setTag('cim_stopper_CY4', 1);
        setTag('cim_stopper_CY3', 0);
        break;
        
        case 5:
        setTag('cim_stopper_CY4', 0);
        break;
            
        case 6:
        setTag("preDisToASRS", getTag("pre_discharge[1].item"));
        setTag('cim_stopper_CY6', 1);
        break;
        
        case 7:
        setTag('cim_lifter_CY7', 1);
        asrs_store();
        break;

        case 8:
        setTag(getTag("ASRS[1].item.location"), 1);
        setTag("ASRSToRawRack", getTag("ASRS[1].item"));
        pick_place(getTag("ASRS[1].item.location"));
        break;

        case 9:
        setTag('cim_lifter_CY7', 0);
        setTag('cim_rbt_KR10_asrs', 0);
        if (raw > 23) {setTag('cim_mod2', 1); setTag('cim_mod1', 0); setTag('cim_counter', 0);}    
        break;
    }
}

function mod2_MRM() {
    if (0 == anime) {
        if (ctr < 25) {
            ctr++;
        } else {
            ctr = 1;
        }
    }
    setTag( 'cim_counter', ctr); 
     
    switch (ctr) {

        case 1:
        setTag('cim_lifter_CY7', 1);
        break;
        
        case 2:
        asrs_retrieve();
        break;
        
        case 3:
        setTag("RawRackToASRS", getTag("raw_rack[1].item"));
        setTag('cim_rbt_KR10_asrs', 0);
        break;
        
        case 4:
        setTag('cim_lifter_CY7', 0);
        break;
        
        case 5:
        setTag('cim_stopper_CY6', 0);
        break;
           
        case 6:
        setTag("ASRSToMach", getTag("ASRS[1].item"));
        setTag('cim_stopper_CY8', 1);
        break;
        
        case 7:
        setTag("MachToCNC", getTag("machining[1].item"));
        setTag('cim_rbt_KR16_machining', 100);
        break;
        
        case 8:
        setTag('cim_rbt_KR16_machining', 180);
        break;
        
        case 9:
        setTag("CNCToWelding", getTag("CNC[1].item"));
        setTag('anime', 1);
        if (run < 3) {
            run++;
            setTag('run', run);
            setTag('cim_rbt_KR16_machining', 320);
            if (1 == run) {
                setTag('cim_rbt_KR16_machining', 280);
                setTag('cim_rbt_KR5_welding', 359);
            }
            if (2 == run) { setTag('cim_rbt_KR5_welding', 0);}
            if (3 == run) { setTag('cim_rbt_KR5_welding', 1);}
        } else {
            run = 0;
            setTag('cim_rbt_KR16_machining', 280);
            setTag("welding[1].item.status", 1);
            setTag('anime', 0);
        }
        setTag('run', run);
        break;
        
        case 10:
        setTag("weldingToMach", getTag("welding[1].item"));
        setTag('cim_rbt_KR16_machining', 0);
        break;
        
        case 11:
        setTag('cim_stopper_CY8', 0);
        break;
        
        case 12:
        setTag("MachToMark", getTag("machining[1].item"));
        setTag('cim_stopper_CY9', 1);
        break;
        
        case 13:
        setTag('cim_lifter_CY10', 1);
        break;
        
        case 14:
        setTag("MarkToBarcode", getTag("marking[1].item"));
        setTag('anime', 1);
        if (run < 2) {
            run++;
            setTag('run', run);
            setTag('cim_rbt_KR5_marking', 91);
            if (1 == run) { setTag('cim_rbt_KR5_marking', 90);}
        } else {
            run = 0;
            setTag('cim_rbt_KR5_marking', 89);
            setTag('anime', 0);
        }
        setTag('run', run);
        break;
        
        case 15:
        setTag("BarcodeToMark", getTag("barcode_print[1].item"));
        setTag('cim_rbt_KR5_marking', 0);
        break;
        
        case 16:
        setTag('cim_lifter_CY10', 0);
        break;
        
        case 17:
        setTag('cim_stopper_CY9', 0);
        break;

        case 18:
        setTag("MarkToPreInfeed", getTag("marking[1].item"));
        setTag('cim_stopper_CY1', 1);  
        break;
        
        case 19:
        setTag("PreInfeedToVS", getTag("pre_infeed[1].item"));
        setTag('anime', 1);
        if (run < 1) {
            run++;
        } else {
            run = 0;
            setTag('anime', 0);
        }
        setTag('run', run);
        setTag('cim_stopper_CY1', 0);
        setTag('cim_stopper_CY3', 1); 
        if (1 == run) {
            setTag('cim_inspection_result', 2);
            barcode = checkbarcode(barcode);
            setTag('cim_barcode', "10"+month+""+date+"A"+barcode);  
            setTag("VSystem[1].item.barcode", getTag("cim_barcode"));
        }
        break;
        
        case 20:
        setTag("VSToPreDis", getTag("VSystem[1].item"));
        setTag('cim_stopper_CY4', 1);
        setTag('cim_stopper_CY3', 0);
        setTag('cim_inspection_result', 0);
        setTag('cim_barcode', "");   
        break;
        
        case 21:
        setTag('cim_stopper_CY4', 0);
        break;
        
        case 22:
        setTag("preDisToASRS", getTag("pre_discharge[1].item"));
        setTag('cim_stopper_CY6', 1);    
        break;
        
        case 23:
        setTag('cim_lifter_CY7', 1);
        asrs_store();
        break;
        
        case 24:
        setTag(getTag("ASRS[1].item.location"), 1);
        setTag("ASRSToFinishRack", getTag("ASRS[1].item"));
        pick_place(getTag("ASRS[1].item.location"));
        break;
        
        case 25:
        setTag('cim_lifter_CY7', 0);
        setTag('cim_rbt_KR10_asrs', 0);
        if (raw == 0 || finish > 23) {setTag('cim_mod3', 1); setTag('cim_mod2', 0); setTag('cim_counter', 0);}
        break;
    }
}
 
function mod3_RAP() {
    if (0 == anime) {
        if (ctr < 11) {
            ctr++;
        } else {
            ctr = 1;
        }
    }
    setTag( 'cim_counter', ctr); 
     
    switch (ctr) {

        case 1:
        setTag('cim_lifter_CY7', 1);
        setTag('cim_stopper_CY6', 1);
        setTag('cim_stopper_CY4', 1);
        break;
        
        case 2:
        asrs_retrieve();
        break;
        
        case 3:
        if (getTag("raw_rack.qty") > 0) setTag("RawRackToASRS", getTag("raw_rack[1].item"));
        else setTag("FinishRackToASRS", getTag("finish_rack[1].item"));
        setTag('cim_rbt_KR10_asrs', 0);
        break;
        
        case 4:
        setTag('cim_lifter_CY7', 0);
        setTag('cim_stopper_CY6', 0);
        break;
        
        case 5:
        setTag("ASRSToMach", getTag("ASRS[1].item"));
        setTag('cim_stopper_CY8', 1);
        break;
            
        case 6:
        setTag('cim_stopper_CY8', 0);
        setTag("MachToMark", getTag("machining[1].item"));
        setTag('cim_stopper_CY9', 1);
        break;
        
        case 7:
        setTag('cim_stopper_CY9', 0);
        setTag("MarkToPreInfeed", getTag("marking[1].item"));
        setTag('cim_stopper_CY1', 1);  
        break;
        
        case 8:
        setTag("PreInfeedToVS", getTag("pre_infeed[1].item"));
        setTag('cim_stopper_CY1', 0);
        setTag('cim_stopper_CY3', 1);  
        break;
        
        case 9:
        setTag("VSToPreDis", getTag("VSystem[1].item"));
        setTag('cim_stopper_CY3', 0);  
        break;
        
        case 10:
        /*setTag('anime', 1);
        if (run < 3) {
            run++;
        } else {
            run = 0;
            setTag('anime', 0);
        }
        
        setTag('run', run);

        if (3 == run ) { setTag('kk_LS4', 1);}*/
        setTag("preDisToDischarge", getTag("pre_discharge[1].item")); 
        setTag('cim_pusher_CY5', 1); 
        break;
        
        case 11:
        setTag('run', run);
        setTag("discharge[1].item"," "); 
        setTag('cim_pusher_CY5', 0); 
        if (raw == 0 && finish == 0) {setTag('cim_mod1', 1); setTag('cim_mod3', 0); setTag('cim_counter', 0);}
        break;
    }
}

function asrs_store() {

/* Mod1 Store start*/
    if (mod1 == 1 && raw < 25) {

    for (var x=0; x < r_list.length; x++)
    {
        list[x] = getTag(r_list[x]);
     
        if (list[0] == 1) data = 1;
        if (list[1] == 1) data = 2;
        if (list[2] == 1) data = 3;
        if (list[3] == 1) data = 4;
        if (list[4] == 1) data = 5;
        if (list[5] == 1) data = 6;
        if (list[6] == 1) data = 7;
        if (list[7] == 1) data = 8;
        if (list[8] == 1) data = 9;
        if (list[9] == 1) data = 10;
        if (list[10] == 1) data = 11;
        if (list[11] == 1) data = 12;
        if (list[12] == 1) data = 13;
        if (list[13] == 1) data = 14;
        if (list[14] == 1) data = 15;
        if (list[15] == 1) data = 16;
        if (list[16] == 1) data = 17;
        if (list[17] == 1) data = 18;
        if (list[18] == 1) data = 19;
        if (list[19] == 1) data = 20;
        if (list[20] == 1) data = 21;
        if (list[21] == 1) data = 22;
        if (list[22] == 1) data = 23;
        if (list[23] == 1) data = 24;
        if (list[24] == 1) data = 25;

        if (data > value) {
        
           if (r_list[x] == "BX4Y5"){
           var value2 = 0
                for (var y=0; y < r_list.length; y++){
                 
                    if (list[y] == 0) {
                      pos = r_list[y];
                      break;
                    }
                }
            } 
           
           else {
           pos = r_list[x+1];
           }
           value = data;
    }
    }

    if (value == 0){
        pos = "AX1Y1"; 
    }

    setTag("ASRS[1].item.location", pos);
    }
/* End of Mod1 store */

/* Mod2 Store start*/
    if (mod2 == 1 && finish < 25) {

    for (var x=0; x < f_list.length; x++)
    {
        list[x] = getTag(f_list[x]);
     
        if (list[0] == 1) data = 1;
        if (list[1] == 1) data = 2;
        if (list[2] == 1) data = 3;
        if (list[3] == 1) data = 4;
        if (list[4] == 1) data = 5;
        if (list[5] == 1) data = 6;
        if (list[6] == 1) data = 7;
        if (list[7] == 1) data = 8;
        if (list[8] == 1) data = 9;
        if (list[9] == 1) data = 10;
        if (list[10] == 1) data = 11;
        if (list[11] == 1) data = 12;
        if (list[12] == 1) data = 13;
        if (list[13] == 1) data = 14;
        if (list[14] == 1) data = 15;
        if (list[15] == 1) data = 16;
        if (list[16] == 1) data = 17;
        if (list[17] == 1) data = 18;
        if (list[18] == 1) data = 19;
        if (list[19] == 1) data = 20;
        if (list[20] == 1) data = 21;
        if (list[21] == 1) data = 22;
        if (list[22] == 1) data = 23;
        if (list[23] == 1) data = 24;
        if (list[24] == 1) data = 25;

      
        if (data > value) {
        
           if (f_list[x] == "BX2Y5"){
           var value2 = 0
                for (var y=0; y < f_list.length; y++){
                 
                    if (list[y] == 0) {
                      pos = f_list[y];
                      break;
                    }
                }
            } 
           
           else {
           pos = f_list[x+1];
           }
           value = data;
    }

    }

    if (value == 0){
        pos = "CX1Y1"; 
    }

    setTag("ASRS[1].item.location", pos);
    }
/* End of Mod2 store */
}

function asrs_retrieve() {
    if (mod2 == 1 && raw > 0) {

    pos = getTag("raw_rack[1].item.location");
    setTag(pos, 0);
    }

    else if (mod3 == 1 && raw > 0) {
        pos = getTag("raw_rack[1].item.location");
        setTag(pos, 0);
    }
    
    else if (mod3 == 1 && finish > 0) {
        pos = getTag("finish_rack[1].item.location");
        setTag(pos, 0);
    }
    
    pick_place(pos);
}

function pick_place(pos) {

    if (pos == "AX1Y1" || pos == "AX1Y2" || pos == "AX1Y3" || pos == "AX1Y4" || pos == "AX1Y5") setTag('cim_rbt_KR10_asrs', 115);
    if (pos == "AX2Y1" || pos == "AX2Y2" || pos == "AX2Y3" || pos == "AX2Y4" || pos == "AX2Y5") setTag('cim_rbt_KR10_asrs', 90);
    if (pos == "AX3Y1" || pos == "AX3Y2" || pos == "AX3Y3" || pos == "AX3Y4" || pos == "AX3Y5") setTag('cim_rbt_KR10_asrs', 70);
    if (pos == "BX3Y1" || pos == "BX3Y2" || pos == "BX3Y3" || pos == "BX3Y4" || pos == "BX3Y5") setTag('cim_rbt_KR10_asrs', 155);
    if (pos == "BX4Y1" || pos == "BX4Y2" || pos == "BX4Y3" || pos == "BX4Y4" || pos == "BX4Y5") setTag('cim_rbt_KR10_asrs', 135);

    if (pos == "CX1Y1" || pos == "CX1Y2" || pos == "CX1Y3" || pos == "CX1Y4" || pos == "CX1Y5") setTag('cim_rbt_KR10_asrs', 245);
    if (pos == "CX2Y1" || pos == "CX2Y2" || pos == "CX2Y3" || pos == "CX2Y4" || pos == "CX2Y5") setTag('cim_rbt_KR10_asrs', 270);
    if (pos == "CX3Y1" || pos == "CX3Y2" || pos == "CX3Y3" || pos == "CX3Y4" || pos == "CX3Y5") setTag('cim_rbt_KR10_asrs', 295);
    if (pos == "BX1Y1" || pos == "BX1Y2" || pos == "BX1Y3" || pos == "BX1Y4" || pos == "BX1Y5") setTag('cim_rbt_KR10_asrs', 225);
    if (pos == "BX2Y1" || pos == "BX2Y2" || pos == "BX2Y3" || pos == "BX2Y4" || pos == "BX2Y5") setTag('cim_rbt_KR10_asrs', 200);
}

function checkTime(j)
{
    if (j<10) j = "00" + j;
    if (9<j && j<100) j = "0" + j;
    return j;
}

function asrs_count(){

    for (var i=0; i<sql.length; i++) {

        var count = getTag(sql[i]);
        if (count == 1) UM++;
        else EP++;
    }

    for (var j=0; j<sql2.length; j++) {

        var count = getTag(sql2[j]);
        if (count == 1) PP++;
        else EP++;
    }

    setTag('cim_UM', UM);
    setTag('cim_PP', PP);
    setTag('cim_EP', EP);
}

function checkDate(i) {
    if (i<10) i="0" + i;
    return i;
}

function checkbarcode(b) {
    
    if(barcode_ctr < 99) setTag('bar_ctr', barcode_ctr);
    else setTag('bar_ctr', 0);
    
    if (barcode_ctr < 10) b = "00" + barcode_ctr;
    else b = barcode_ctr;
   
   return b;
}

