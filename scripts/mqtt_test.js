(function() {
	var jsonString = getTag("mqtt_tag");
	var obj = JSON.parse(jsonString);
 // Only works if active x is disabled
	var i;
	for (i in obj) { // i is the name of each name-value pair in the object obj
		setTag("mqtt_" + i, obj[i]); // e.g. assume each virtual tag name is mqtt_<name>
	}
})();