var boollist = {
'bool_a': 0,
'bool_b': 0,
'bool_c': 0,
'bool_d': 0,
'bool_e': 0,
'bool_f': 0
};


for (var j in boollist) {
    var bool = getTag(j) || Math.random()*boollist[j];
    if((j=='bool_b')||(j=='bool_c')||(j=='bool_f'))
    	bool = getInvertedBoolean( bool, boollist[j]);
    else
		bool = getBoolean( bool, boollist[j]);
	setTag( j , bool);
	debugTag( j); //this line print value of tag in debugging mode.
}

function getBoolean( prev, range) {
    var rnd = Math.random();
	if ((rnd)> 0.5) {
	    rnd = 1;
	}
	else {
	    rnd = 0;
    }
    return rnd;
}

function getInvertedBoolean( prev, range) {
    var rnd = Math.random();
	if ((rnd)> 0.5) {
	    rnd = -1;
	}
	else {
	    rnd = 0;
    }
    return rnd;
}